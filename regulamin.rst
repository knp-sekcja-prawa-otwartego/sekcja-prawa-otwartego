Regulamin pracy Sekcji Prawa Otwartego
======================================

Przepisy ogólne
---------------

1. Sekcja Prawa Otwartego (zwana dalej „Sekcją”) jest sekcją tematyczną
   o zasięgu ogólnopolskim, działającą w ramach partii politycznej
   Kongres Nowej Prawicy (zwanej dalej „KNP”).
2. Celem Sekcji jest zarządzanie procesem tworzenia i analizy tekstów
   prawnych oraz dokumentów programowych.
3. Prezes KNP oraz Sekretarz Rady Głównej mogą rozwiązać Sekcję.

Repozytoria
-----------

4. Prace nad tekstami prawnymi odbywają się w ramach repozytoriów
   systemu kontroli wersji Git, zwanych dalej „repozytoriami”.
5. Commity i gałęzie wchodzące w skład repozytoriów mogą mieć charakter
   roboczy. Repozytoria mogą zawierać wzajemnie wykluczające się
   commity.
6. Tagi muszą być podpisane za pomocą PGP.
7. Poprawne otagowanie commita jest rozumiane jako poświadczenie, iż
   wyznacza on obowiązującą wersję dokumentu bądź stanowi oficjalną
   propozycję KNP.

Repozytorium Sekcji
-------------------

8. Dokumenty i informacje dotyczące Sekcji są gromadzone w publicznym
   repozytorium, zwanym dalej Repozytorium Sekcji.
9. Z wyjątkiem p. 2, każda formalna decyzja dotycząca powołanej Sekcji
   wchodzi w życie w momencie opublikowania jej w Repozytorium Sekcji
   w commicie podpisanym za pomocą PGP przez osobę upoważnioną do jej
   wydania.

Organizacja Sekcji
------------------

10. Pracami Sekcji kieruje jej przewodniczący, zwany dalej
    „Przewodniczącym”. Przewodniczący może delegować część swoich
    kompetencyj na innych członków Sekcji.
11. Prezes KNP oraz Sekretarz Rady Głównej mogą zmienić
    Przewodniczącego, a w razie powstania wakatu – powołać nowego.
12. Przewodniczący, ustępując samodzielnie ze stanowiska, może
    mianować nowego Przewodniczącego.
13. Przewodniczący może zmienić regulamin pracy Sekcji.
14. Lista członków Sekcji wraz z odciskami palców ich kluczy PGP jest
    przechowywana w Repozytorium Sekcji.
15. Akces do Sekcji następuje wskutek włączenia przez Przewodniczącego
    do Repozytorium Sekcji commita dodającego do listy członków Sekcji
    wpis z danymi członka, przy czym commit ten musi być podpisany za
    pomocą PGP przez samego kandydata.
16. Usunięcie z Sekcji następuje wskutek usunięcia przez
    Przewodniczącego z listy członków Sekcji wpisu dotyczącego danego
    członka.
17. Początkowy skład Sekcji wraz z Przewodniczącym oraz początkowe
    Repozytorium Sekcji są ustalane w treści pisma zawierającego
    wniosek o zatwierdzenie regulaminu pracy sekcji.
