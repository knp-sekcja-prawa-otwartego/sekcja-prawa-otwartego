Sekcja Prawa Otwartego
======================

Sekcja Prawa Otwartego jest sekcją tematyczną działającą w oparciu o p. 93 statutu Kongresu Nowej Prawicy, której celem jest zarządzanie procesem tworzenie i analizy tekstów prawnych oraz dokumentów programowych. Prace odbywające się w oparciu o sekcję mają charakter publiczny. Więcej na ten temat w `manifeście sekcji <manifest.rst>`__. Dostępny jest również `regulamin <regulamin.rst>`__.

Jak pomóc
*********

Wybierz jedno z repozytoriów_, nanieś poprawki, zgłoś pull requesta lub skomentuj istniejącego.

(Tę część należy rozbudować tak, aby stanowiła przystępne wprowadzenie dla nieprogramistów).

.. _repozytoriów:

Repozytoria
***********

- `Ustawa o drogach publicznych <http://bitbucket.org/knp-sekcja-prawa-otwartego/ustawa-o-drogach-publicznych/>`__

Skład
*****

+-----------------------------+--------------------------------+---------------------------------------------+--------------------------------------------------+
|Członek                      |Uprawnienia                     |Email                                        |Odcisk klucza PGP                                 |
+=============================+================================+=============================================+==================================================+
|Krzysztof Jurewicz           |przewodniczący                  |krzysztof.jurewicz@gmail.com                 |49B8 FA87 441C CF12 268D A7B1 2483 F9A8 5ECD 3D48 |
+-----------------------------+--------------------------------+---------------------------------------------+--------------------------------------------------+
|Mieczysław Burchert          |                                |                                             |                                                  |
+-----------------------------+--------------------------------+---------------------------------------------+--------------------------------------------------+
|Kamil Frydlewicz             |                                |                                             |                                                  |
+-----------------------------+--------------------------------+---------------------------------------------+--------------------------------------------------+

Pytania i odpowiedzi
********************

Czy trzeba być członkiem KNP, żeby zgłosić swoje zmiany?
  Nie.
Dlaczego korzystacie akurat z Bitbucketa?
  `Github posiada błąd w wyświetlaniu numeracji list w plikach RST <https://github.com/github/markup/issues/390>`__, co ma znaczenie w przypadku tekstów prawnych. `Ten sam błąd występuje w Gitlabie <https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/322>`__.
